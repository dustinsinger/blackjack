function Deck(){
    var deck = [
       new Card(2,"assets/2_of_clubs.png"),
       new Card(2,"assets/2_of_diamonds.png"),
       new Card(2,"assets/2_of_hearts.png"),
       new Card(2,"assets/2_of_spades.png"),
       new Card(3,"assets/3_of_clubs.png"),
       new Card(3,"assets/3_of_diamonds.png"),
       new Card(3,"assets/3_of_hearts.png"),
       new Card(3,"assets/3_of_spades.png"),
       new Card(4,"assets/4_of_clubs.png"),
       new Card(4,"assets/4_of_diamonds.png"),
       new Card(4,"assets/4_of_hearts.png"),
       new Card(4,"assets/4_of_spades.png"),
       new Card(5,"assets/5_of_clubs.png"),
       new Card(5,"assets/5_of_diamonds.png"),
       new Card(5,"assets/5_of_hearts.png"),
       new Card(5,"assets/5_of_spades.png"),
       new Card(6,"assets/6_of_clubs.png"),
       new Card(6,"assets/6_of_diamonds.png"),
       new Card(6,"assets/6_of_hearts.png"),
       new Card(6,"assets/6_of_spades.png"),
       new Card(7,"assets/7_of_clubs.png"),
       new Card(7,"assets/7_of_diamonds.png"),
       new Card(7,"assets/7_of_hearts.png"),
       new Card(7,"assets/7_of_spades.png"),
       new Card(8,"assets/8_of_clubs.png"),
       new Card(8,"assets/8_of_diamonds.png"),
       new Card(8,"assets/8_of_hearts.png"),
       new Card(8,"assets/8_of_spades.png"),
       new Card(9,"assets/9_of_clubs.png"),
       new Card(9,"assets/9_of_diamonds.png"),
       new Card(9,"assets/9_of_hearts.png"),
       new Card(9,"assets/9_of_spades.png"),
       new Card(10,"assets/10_of_clubs.png"),
       new Card(10,"assets/10_of_diamonds.png"),
       new Card(10,"assets/10_of_hearts.png"),
       new Card(10,"assets/10_of_spades.png"),
       new Card(11,"assets/ace_of_clubs.png"),
       new Card(11,"assets/ace_of_diamonds.png"),
       new Card(11,"assets/ace_of_hearts.png"),
       new Card(11,"assets/ace_of_spades.png"),
       new Card(10,"assets/jack_of_clubs.png"),
       new Card(10,"assets/jack_of_diamonds.png"),
       new Card(10,"assets/jack_of_hearts.png"),
       new Card(10,"assets/jack_of_spades.png"),
       new Card(10,"assets/king_of_clubs.png"),
       new Card(10,"assets/king_of_diamonds.png"),
       new Card(10,"assets/king_of_hearts.png"),
       new Card(10,"assets/king_of_spades.png"),
       new Card(10,"assets/queen_of_clubs.png"),
       new Card(10,"assets/queen_of_diamonds.png"),
       new Card(10,"assets/queen_of_hearts.png"),
       new Card(10, "assets/queen_of_spades.png"),
     ];
     return deck;
}

function Card(value, image){
     this.value = value;
     this.image = image;
}

currentDeck = new Deck();
cardBack = "<img src='"+"assets/cardback.png"+"' >";
totalCash = 1000;

function placeBet(){
     bet = parseInt(document.getElementById('bet').value);
     console.log(bet);
}


function deal(){
     if (document.getElementById('bet').value === ""){
          alert("Please enter a bet");
     }
     else if (bet > totalCash){
          alert("You do not have enough cash. Please enter a valid bet");
     }
     else {
          document.getElementById("hit").disabled = false;
          document.getElementById("stand").disabled = false;
          if(currentDeck.length <= 8){
            currentDeck = new Deck();
          }
          document.getElementById('dealer').innerHTML = "";
          document.getElementById('player').innerHTML = "";


          for (var i = 0; i < 2; i++){
               var random = Math.floor(Math.random() * currentDeck.length);
               var card = currentDeck[random];
               currentDeck.splice(random, 1);
               //console.log(currentDeck.length);

               if(i === 0){
                 dealerCard1Value = card.value;
                 dealerCard1Image = card.image;
                 //console.log(dealerCard1Value);
                 document.getElementById('dealer').innerHTML += ("<img src='"+dealerCard1Image+"' >");
               }
               else if (i === 1){
                 dealerCard2Value = card.value;
                 dealerCard2Image = card.image;
                 document.getElementById('dealer').innerHTML += (cardBack);
                 //console.log(dealerCard2Value);
               }
               if (dealerCard1Value === 11 && dealerCard2Value === 11){
                    dealerCard2Value = 1;
               }
          }
          for (var j = 0; j < 2; j++){
               var random = Math.floor(Math.random() * currentDeck.length);
               var card = currentDeck[random];
               currentDeck.splice(random, 1);
               //console.log(currentDeck.length);
               document.getElementById('player').innerHTML += ("<img src='"+card.image+"' >");
               if(j === 0){
                 playerCard1Value = card.value;
                 //console.log(playerCard1Value);
               }
               else if (j === 1){
                 playerCard2Value = card.value;
                 //console.log(playerCard2Value);
               }
               if (playerCard1Value === 11 && playerCard2Value === 11){
                    playerTotal -= 10;
                    playerCard1Value = 1;
               }
          }

          dealerTotal = dealerCard1Value;
          playerTotal = playerCard1Value + playerCard2Value;

          if (dealerCard1Value + dealerCard2Value === 21){
               dealerTotal = dealerCard1Value + dealerCard2Value
               document.getElementById('dealer').innerHTML = ("<img src='"+dealerCard1Image+"' >");
               document.getElementById('dealer').innerHTML += ("<img src='"+dealerCard2Image+"' >");
               dealerTotal  += ": Dealer Wins!";
               playerTotal += ": You Lose :(";
               document.getElementById("hit").disabled = true;
               document.getElementById("stand").disabled = true;
               totalCash -= bet;
          }
     }
}

function betTally(){
     if (document.getElementById('bet').value === ""){
          document.getElementById('dealer').innerHTML = "";
          document.getElementById('player').innerHTML = "";
     }
     else if (bet > totalCash){
          document.getElementById('dealer').innerHTML = "";
          document.getElementById('player').innerHTML = "";
     }
     else {
          tally();
     }
     if (totalCash === 0){
          alert("No more cash. Game Over");
          location.reload();
     }
}

function tally(){
     document.getElementById('dealerTotal').innerHTML = dealerTotal;
     document.getElementById('playerTotal').innerHTML = playerTotal;
     document.getElementById('totalCash').innerHTML = "Total: $" + totalCash;

}

function hit(){
     var random = Math.floor(Math.random() * currentDeck.length);
     var card = currentDeck[random];
     var hitCardValue = card.value;

     if (hitCardValue === 11 && playerTotal > 21){
          hitCardValue = 1;
     }
     playerTotal += hitCardValue;
     if (playerTotal > 21 && playerCard1Value === 11){
          playerTotal -= 10;
          playerCard1Value = 1;
     }
     if (playerTotal > 21 && playerCard2Value === 11){
          playerTotal -= 10;
          playerCard2Value = 1;
     }
     console.log(playerTotal)

     console.log(hitCardValue);
     if (playerTotal > 21){
          playerTotal += " Bust... You lose";
          dealerTotal = dealerCard1Value + dealerCard2Value;
          dealerTotal += ": Dealer Wins";
          document.getElementById('dealer').innerHTML = ("<img src='"+dealerCard1Image+"' >");
          document.getElementById('dealer').innerHTML += ("<img src='"+dealerCard2Image+"' >");
          document.getElementById("hit").disabled = true;
          document.getElementById("stand").disabled = true;
          totalCash -= bet;
     }

     currentDeck.splice(random, 1);
     //console.log(currentDeck.length);
     document.getElementById('player').innerHTML += ("<img src='"+card.image+"' >");
}

function stand() {
     dealerTotal = dealerCard1Value + dealerCard2Value;
     document.getElementById('dealer').innerHTML = ("<img src='"+dealerCard1Image+"' >");
     document.getElementById('dealer').innerHTML += ("<img src='"+dealerCard2Image+"' >");
     while (dealerTotal < 17){

          var random = Math.floor(Math.random() * currentDeck.length);
          var card = currentDeck[random];
          currentDeck.splice(random, 1);
          document.getElementById('dealer').innerHTML += ("<img src='"+card.image+"' >");
          if(card.value === 11 && dealerTotal < 17){
               dealerTotal += 1;
          }
          else{
               dealerTotal += card.value;
          }
          if (dealerTotal > 21 && (dealerCard1Value === 11)){
               dealerTotal -= 10;
               dealerCard1Value = 1;
          }
          if (dealerTotal > 21 && (dealerCard2Value === 11)){
               dealerTotal -= 10;
               dealerCard2Value = 1;
          }


               //console.log(dealerTotal);


     }
     if (dealerTotal > playerTotal && dealerTotal <= 21){
          dealerTotal += ": Dealer Wins!";
          playerTotal += ": You Lose :(";
          totalCash -= bet;
     }
     else if (playerTotal > dealerTotal){
          dealerTotal += ": Dealer Loses";
          playerTotal += ": You Win!";
          totalCash = totalCash + bet;
     }
     else if (dealerTotal > 21){
          dealerTotal += ": Dealer Busts";
          playerTotal += ": You Win!";
          totalCash = totalCash + bet;
     }
     else if (playerTotal === dealerTotal){
          dealerTotal += ": Push";
          playerTotal += ": Push";
          totalCash = totalCash;
     }
     document.getElementById("hit").disabled = true;
     document.getElementById("stand").disabled = true;
}
